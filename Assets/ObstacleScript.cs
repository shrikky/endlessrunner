﻿using UnityEngine;
using System.Collections;
namespace UnitySampleAssets._2D
{
public class ObstacleScript : MonoBehaviour {

	public float speed;
	public Transform player;
	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
		
		speed = player.GetComponent<PlayerBehavior>().getSpeed();
		speed = (speed*100f);
		this.transform.position -= new Vector3(speed*Time.deltaTime,0,0);
	
	}
}

}
