﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;

namespace UnitySampleAssets._2D
{

    [RequireComponent(typeof(PlatformerCharacter2D))]
    public class PlayerBehavior : MonoBehaviour
    {
        //CharacterController cc;
        //Vector2 lastInput;
        float lastleftInput = 0;
        float lastRightInput = 0;
        int leftPressCount = 0;
        int rightPressCount = 0;
        public float startSpeed = 0.05f;
        float speed = 0.1f;


        private PlatformerCharacter2D character;
        private bool jump;

        private void Awake()
        {
            character = GetComponent<PlatformerCharacter2D>();
        }


        //// Use this for initialization
        //void Start()
        //{
        //    cc = gameObject.GetComponent<CharacterController>();
        //}

        // Update is called once per frame
        void Update()
        {


            // Left Trigger goes from 0 <= x <= 1, -1 <= y <= 0
            float leftTriggerInput = Input.GetAxis("Horizontal");
            float rightTriggerInput = Input.GetAxis("Vertical");
            if (leftTriggerInput <= 0 && lastleftInput > 0)
            {
                leftPressCount++;
                // Press LT after RT
                Debug.Log("LT");
            }
            else if (rightTriggerInput <= 0 && lastRightInput > 0)
            {
                rightPressCount++;
                // Pressed RT after LT
                Debug.Log("RT");
            }

            if (leftTriggerInput == 0 && lastleftInput == 0 && rightTriggerInput == 0 && lastRightInput == 0)
            {
                if (speed > 0)
                {
                    speed -= 0.1f;
                }
                leftPressCount = rightPressCount = 0;
            }
            else if (Mathf.Abs(rightPressCount - leftPressCount) > 1)
            {
                if (speed > 0)
                {
                    speed -= 0.1f;
                }
                leftPressCount = rightPressCount = 0;
            }
            else if (rightPressCount != 0 && rightPressCount == leftPressCount)
            {
                Debug.Log("Speed up");
                speed += 1;
                leftPressCount = rightPressCount = 0;
            }

            if (!jump)
                // Read the jump input in Update so button presses aren't missed.
                jump = CrossPlatformInputManager.GetButtonDown("Jump");

            //Debug.Log(transform.position);

            lastleftInput = leftTriggerInput;
            lastRightInput = rightTriggerInput;
        }

        private void FixedUpdate()
        {
            // Read the inputs.
            //bool crouch = Input.GetKey(KeyCode.LeftControl);
            //float h = CrossPlatformInputManager.GetAxis("Horizontal");

            // Pass all parameters to the character control script.

            //            cc.Move(speed * Time.deltaTime);
            character.Move(speed * Time.deltaTime + startSpeed, false, jump);
            jump = false;
        }

        public float getSpeed()
        {
            return (speed * Time.deltaTime) + startSpeed;

        }
    }
}